# Capataz

This project is my first attempt at learning Go and how concurrency semantics
work in the language. I did implement a simple supervision tree utility that can
manage multiple goroutines and restart them in case of failures.

There are both unit and property tests (courtesy of gopter) in the test-suite.

There is an example of usage in the `example/main.go` file
