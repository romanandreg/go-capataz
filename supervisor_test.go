// READ THIS FILE FROM BOTTOM TO TOP
package capataz

import (
	"context"
	"sync"

	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/leanovate/gopter"
	"github.com/leanovate/gopter/gen"
	"github.com/leanovate/gopter/prop"
)

////////////////////////////////////////////////////////////////////////////////
// UTILITIES
////////////////////////////////////////////////////////////////////////////////

// From: https://stackoverflow.com/a/20396392
// ChanToSlice reads all data from ch (which must be a chan), returning a
// slice of the data. If ch is a 'T chan' then the return value is of type
// []T inside the returned interface.
// A typical call would be sl := ChanToSlice(ch).([]int)
func ChanToSlice(ch interface{}) interface{} {
	chv := reflect.ValueOf(ch)
	slv := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(ch).Elem()), 0, 0)
	for {
		v, ok := chv.Recv()
		if !ok {
			return slv.Interface()
		}
		slv = reflect.Append(slv, v)
	}
}

// Action that keeps an internal state that keeps failure count, it fails after
// a N errors, and then calls onDone with some leeway time
func failTimes(n int, onDone func()) func(context.Context) error {
	acc := 0
	return func(_ctx context.Context) error {
		time.Sleep(5 * time.Millisecond)
		if acc < n {
			acc += 1
			return fmt.Errorf("Faulty Worker")
		} else {
			onDone()
			// give some leeway to the supervisor
			time.Sleep(50 * time.Millisecond)
			return nil
		}
	}
}

func failTimesAndWait(n int, onDone func()) func(context.Context) error {
	acc := 0
	return func(ctx context.Context) error {
		time.Sleep(5 * time.Millisecond)
		if acc < n {
			acc += 1
			return fmt.Errorf("Faulty Worker")
		} else {
			onDone()
			<-ctx.Done()
			// give some leeway to the supervisor
			time.Sleep(100 * time.Microsecond)
			return ctx.Err()
		}
	}
}

func expectCallTimes(n int) (callArgumets func() []interface{}, callFn func(arg interface{})) {
	var acc []interface{}
	return func() []interface{} {
			return acc
		}, func(arg interface{}) {
			acc = append(acc, arg)
		}
}

func panicTimes(n int, onDone func()) func(context.Context) error {
	acc := 0
	return func(ctx context.Context) error {
		time.Sleep(10 * time.Microsecond)
		if acc < n {
			acc += 1
			panic("Faulty Worker")
		} else {
			onDone()
			<-ctx.Done()
			return ctx.Err()
		}

	}
}

func completeTimesAndHold(n int, onDone func()) func(context.Context) error {
	acc := 0
	return func(ctx context.Context) error {
		time.Sleep(10 * time.Microsecond)
		if acc < n {
			acc += 1
			return nil
		} else {
			onDone()
			<-ctx.Done()
			return ctx.Err()
		}
	}
}

// Similar to testSupervisor, but this one returns a slice with the ProcessEvents
func collectSupervisorEvents(blockBeforeStop func(), opts SupervisorOptions) []ProcessEvent {
	var wg sync.WaitGroup
	var result []ProcessEvent
	evChan := make(chan ProcessEvent)

	_, stopSupervisor := NewSupervisor1(evChan, opts)

	wg.Add(1)
	go func() {
		result = ChanToSlice(evChan).([]ProcessEvent)
		wg.Done()
	}()

	blockBeforeStop()
	stopSupervisor()

	// Give some leeway for events to come to the event chan
	time.Sleep(100 * time.Microsecond)
	close(evChan)
	wg.Wait()
	return result
}

func testSupervisorRaw(
	delayFn func(func()),
	opts SupervisorOptions,
	modFn func(Supervisor),
	expected []func(ProcessEvent) error,
) error {
	var wg sync.WaitGroup
	var err error
	evChan := make(chan ProcessEvent)

	supervisor, stopSupervisor := NewSupervisor1(evChan, opts)

	wg.Add(1)
	go func() {
		result := ChanToSlice(evChan).([]ProcessEvent)
		if len(result) < len(expected) {
			err = fmt.Errorf("Got event list of size %d, expected of size %d", len(result), len(expected))
		}
		for i, f := range expected {
			ev := result[i]
			if err1 := f(ev); err1 != nil {
				err = err1
				break
			}
		}
		wg.Done()
	}()

	modFn(supervisor)
	delayFn(stopSupervisor)

	// Give some leeway for events to come to the event chan
	time.Sleep(100 * time.Microsecond)
	close(evChan)
	wg.Wait()
	return err
}

// Starts a supervisor with the given options until a delay amount of time, and
// then it collects all the events from the system, we use asserters on this
// events to test things are working as expected.
//
// This testing may be error-prone given we are matching events as they happen,
// and concurrency can alter the order of events without making the test
// invalid, given this, this function should be used to test simple scenarios
func testSupervisor(
	delayFn func(),
	opts SupervisorOptions,
	expected []func(ProcessEvent) error,
) error {
	return testSupervisorRaw(
		func(stopSupervisor func()) {
			delayFn()
			stopSupervisor()
		},
		opts,
		func(_ Supervisor) {}, expected)
}

func joinSupervisor(
	opts SupervisorOptions,
	expected []func(ProcessEvent) error,
) error {
	return testSupervisorRaw(
		func(_ func()) {},
		opts,
		func(s Supervisor) {
			s.Join()
		},
		expected,
	)
}

// Event matcher that asserts the name and status of an event
func statusMatches(expectedName string, expectedStatus ProcessEventTag) func(ProcessEvent) error {
	return func(ev ProcessEvent) error {
		if ev.Name() != expectedName {
			return fmt.Errorf("Expecting name to be %s, but was %s", expectedName, ev.Name())
		}
		if ev.Status() != expectedStatus {
			return fmt.Errorf("Expecting status to be %s, but was %s", expectedStatus, ev.Status())
		}
		return nil
	}
}

////////////////////////////////////////////////////////////////////////////////
// GENERATORS
////////////////////////////////////////////////////////////////////////////////

var stableWorkerOptionsGen gopter.Gen = gen.IntRange(1, 1000).Map(func(n int) WorkerOptions {
	return WorkerOptions{
		Name: fmt.Sprintf("Worker %d", n),
		Action: func(ctx context.Context) error {
			<-ctx.Done()
			return ctx.Err()
		},
		RestartStrategy: Permanent,
	}
})

func supervisorOptionsGen(workerGen gopter.Gen, strategy SupervisorRestartStrategyTag) gopter.Gen {
	return gen.IntRange(0, 20).FlatMap(func(n0 interface{}) gopter.Gen {
		n := n0.(int)
		return gen.SliceOfN(n, workerGen).
			Map(func(workers []WorkerOptions) SupervisorOptions {
				return SupervisorOptions{
					Name:            fmt.Sprintf("Supervisor %d", n),
					BufferSize:      1000,
					Intensity:       5,
					Period:          20 * time.Second,
					RestartStrategy: strategy,
					Workers:         workers,
				}
			})
	}, reflect.TypeOf(SupervisorOptions{}))
}

// Generates supervisors with many workers and only one faulty worker that fails once
func faultyTransientWorkerSupervisor() (gopter.Gen, *sync.WaitGroup) {
	var wg sync.WaitGroup
	wg.Add(1)
	return gen.IntRange(0, 20).FlatMap(func(n0 interface{}) gopter.Gen {
		n := n0.(int)
		return gen.SliceOfN(n, stableWorkerOptionsGen).
			Map(func(workers0 []WorkerOptions) SupervisorOptions {
				workers := append(workers0, WorkerOptions{
					Name:            fmt.Sprintf("Faulty Worker %d", n),
					Action:          failTimes(1, func() { wg.Done() }),
					RestartStrategy: Transient,
				})
				return SupervisorOptions{
					Name:            fmt.Sprintf("Supervisor %d", n),
					BufferSize:      1000,
					Intensity:       5,
					Period:          20 * time.Second,
					RestartStrategy: AllForOne,
					Workers:         workers,
				}
			})
	}, reflect.TypeOf(SupervisorOptions{})), &wg

}

////////////////////////////////////////////////////////////////////////////////
// PROPERTY TESTS
////////////////////////////////////////////////////////////////////////////////

func TestEveryWorkerIsStopped(t *testing.T) {
	properties := gopter.NewProperties(nil)
	properties.Property("Every started worker event is matched with a stopped worker event", prop.ForAll(
		func(opts SupervisorOptions) bool {
			events := collectSupervisorEvents(func() { time.Sleep(10 * time.Millisecond) }, opts)
			acc := 0
			for _, ev := range events {
				if ev.Status() == STARTED {
					acc = acc + 1
				} else if ev.Status() == CANCELED {
					acc = acc - 1
				}
			}
			return acc == 0
		},
		supervisorOptionsGen(stableWorkerOptionsGen, OneForOne),
	))
	properties.TestingRun(t)
}

// This function tests AllForOne behavior
func TestEveryFailTriggersSiblingCancel(t *testing.T) {
	properties := gopter.NewProperties(nil)
	supervisorGen, waitGroup := faultyTransientWorkerSupervisor()
	properties.Property("Every failed worker event is matched with n - 1 cancel events", prop.ForAll(
		func(opts SupervisorOptions) bool {
			workerCount := len(opts.Workers)
			failedCount := 0
			cancelCount := 0
			startedCount := 0
			events := collectSupervisorEvents(func() { waitGroup.Wait() }, opts)
			for _, ev := range events {
				// fmt.Printf("STATUS: %s; NAME: %s\n", ev.Status(), ev.Name())
				if ev.Status() == FAILED {
					failedCount = failedCount + 1
				} else if ev.Status() == CANCELED {
					cancelCount = cancelCount + 1
				} else if ev.Status() == STARTED {
					startedCount = startedCount + 1
				}
			}

			if workerCount == 1 {
				return true
			} else if failedCount > 0 {
				// minus supervisor
				workerCancelCount := cancelCount - 1
				// minus faulty worker
				nonFaultyWorkerCount := workerCount - 1
				// rounds should be two
				rounds := workerCancelCount / nonFaultyWorkerCount
				// minus faulty worker expected starts and supervisor start
				workerStartedCount := startedCount - 2
				// if more than one round, take out the second
				// start from faulty
				if rounds > 1 {
					workerStartedCount = workerStartedCount - 1
				}

				// fmt.Printf("=========\n")
				// fmt.Printf("> rounds: %d\n", rounds)
				// fmt.Printf("> failedCount: %d\n", failedCount)
				// fmt.Printf("> nonFaultyWorkerCount: %d\n", nonFaultyWorkerCount)
				// fmt.Printf("> workerStartedCount: %d\n", workerStartedCount)
				// fmt.Printf("> workerCancelCount: %d\n", workerCancelCount)
				// fmt.Printf("=========\n")
				cancelPerStart := workerStartedCount == workerCancelCount
				moreThanOneStart := workerStartedCount/nonFaultyWorkerCount == rounds
				return cancelPerStart && moreThanOneStart
			} else {
				return true
			}

		},
		supervisorGen,
	))
	properties.TestingRun(t)
}

////////////////////////////////////////////////////////////////////////////////
// UNIT TESTS
////////////////////////////////////////////////////////////////////////////////

func TestSupervisorStartsAndStops(t *testing.T) {
	err := testSupervisor(
		func() { time.Sleep(100 * time.Microsecond) },
		SupervisorOptions{
			Name:            "Test Supervisor",
			Intensity:       5,
			Period:          1 * time.Second,
			RestartStrategy: OneForOne,
		},
		[]func(ProcessEvent) error{
			statusMatches("Test Supervisor", STARTED),
			statusMatches("Test Supervisor", CANCELED),
		},
	)
	if err != nil {
		t.Error(err)
	}
}

//

func terminationOrderTest(
	t *testing.T,
	terminationOrder SupervisorTerminationOrder,
	matchers []func(ProcessEvent) error,
) {
	// replicate this at least 100 times to reduce chances
	// of false positives given possible race conditions
	var wg sync.WaitGroup
	workerAction1 := completeTimesAndHold(0, func() { wg.Done() })
	workerAction2 := completeTimesAndHold(0, func() { wg.Done() })
	wg.Add(2)
	err := testSupervisor(
		func() {
			wg.Wait()
			// give the supervisor some leeway to handle termination
			time.Sleep(100 * time.Microsecond)
		},
		SupervisorOptions{
			Name:             "Test Supervisor",
			Intensity:        5,
			Period:           1 * time.Second,
			RestartStrategy:  OneForOne,
			TerminationOrder: terminationOrder,
			// ^^^ IMPORTANT CHANGE
			Workers: []WorkerOptions{
				WorkerOptions{
					Name:            "Worker 1",
					Action:          workerAction1,
					RestartStrategy: Transient,
				},
				WorkerOptions{
					Name:            "Worker 2",
					Action:          workerAction2,
					RestartStrategy: Transient,
				},
			},
		},
		matchers,
	)
	if err != nil {
		t.Error(err)
	}
}

func TestLeftToRightTerminationOrder(t *testing.T) {
	terminationOrderTest(
		t,
		LeftToRight,
		[]func(ProcessEvent) error{
			statusMatches("Test Supervisor", STARTED),
			statusMatches("Worker 1", STARTED),
			statusMatches("Worker 2", STARTED),
			statusMatches("Worker 1", CANCELED),
			statusMatches("Worker 2", CANCELED),
			statusMatches("Test Supervisor", CANCELED),
		},
	)
}

func TestRightToLeftTerminationOrder(t *testing.T) {
	terminationOrderTest(
		t,
		RightToLeft,
		[]func(ProcessEvent) error{
			statusMatches("Test Supervisor", STARTED),
			statusMatches("Worker 1", STARTED),
			statusMatches("Worker 2", STARTED),
			statusMatches("Worker 2", CANCELED),
			statusMatches("Worker 1", CANCELED),
			statusMatches("Test Supervisor", CANCELED),
		},
	)
}

func TestRescuePanicWorkers(t *testing.T) {
	var wg sync.WaitGroup
	workerAction := panicTimes(1, func() { wg.Done() })
	wg.Add(1)
	err := testSupervisor(
		func() {
			wg.Wait()
			// give the supervisor some leeway
			time.Sleep(100 * time.Microsecond)
		},
		SupervisorOptions{
			Name:            "Test Supervisor",
			Intensity:       5,
			Period:          1 * time.Second,
			RestartStrategy: OneForOne,
			RescuePanic:     true,
			// ^^^ IMPORTANT CHANGE
			Workers: []WorkerOptions{
				WorkerOptions{
					Name:            "Panic Worker",
					Action:          workerAction,
					RestartStrategy: Transient,
				},
			},
		},
		[]func(ProcessEvent) error{
			statusMatches("Test Supervisor", STARTED),
			statusMatches("Panic Worker", STARTED),
			statusMatches("Panic Worker", FAILED),
			statusMatches("Panic Worker", STARTED),
			// shutdown of supervisor
			statusMatches("Panic Worker", CANCELED),
			statusMatches("Test Supervisor", CANCELED),
		},
	)
	if err != nil {
		t.Error(err)
	}
}

func TestWorkerBackoffOnError(t *testing.T) {
	var wg sync.WaitGroup
	workerAction := failTimesAndWait(3, func() { wg.Done() })
	getCallArguments, callFn := expectCallTimes(3)
	wg.Add(1)
	err := testSupervisor(
		func() {
			wg.Wait()
			// give the supervisor some leeway
			time.Sleep(100 * time.Microsecond)
		},
		SupervisorOptions{
			Name:            "Test Supervisor",
			Intensity:       5,
			Period:          10 * time.Second,
			RestartStrategy: OneForOne,
			Workers: []WorkerOptions{
				WorkerOptions{
					Name:   "Fail Worker",
					Action: workerAction,
					BackoffOnError: func(err error, restartCount int64) time.Duration {
						callFn(restartCount)
						// don't need to wait to assert test
						return 0
					},
					RestartStrategy: Transient,
				},
			},
		},
		[]func(ProcessEvent) error{
			statusMatches("Test Supervisor", STARTED),
			// 1
			statusMatches("Fail Worker", STARTED),
			statusMatches("Fail Worker", FAILED),
			// 2
			statusMatches("Fail Worker", STARTED),
			statusMatches("Fail Worker", FAILED),
			// 3
			statusMatches("Fail Worker", STARTED),
			statusMatches("Fail Worker", FAILED),
			// 4
			statusMatches("Fail Worker", STARTED),
			// shutdown of supervisor
			statusMatches("Fail Worker", CANCELED),
			statusMatches("Test Supervisor", CANCELED),
		},
	)
	// assert lifecycle is as expected
	if err != nil {
		t.Error(err)
	}
	// assert the backoff function got called 3 times with different restart
	// count each time
	expected := []int64{0, 1, 2}
	result := getCallArguments()
	for i, current := range result {
		if current.(int64) != expected[i] {
			t.Errorf("Backoff function doesn't have right input arguments: got %d; want %d", current, expected[i])
		}
	}
}

func TestWorkerCancelTimeout(t *testing.T) {
	defer func() {
		err, ok := recover().(error)
		if ok && err != nil {
			if !strings.HasSuffix(err.Error(), "didn't terminate on cancel after timeout") {
				t.Errorf("Expecting to raise error around timeout, but didn't: %s", err.Error())
			}
		}
	}()
	var forever sync.WaitGroup
	// this worker is never going to cancel
	workerAction := failTimesAndWait(0, func() { forever.Wait() })
	err := testSupervisor(
		func() {
			// give the supervisor some leeway
			time.Sleep(10 * time.Millisecond)
		},
		SupervisorOptions{
			Name:            "Test Supervisor",
			Intensity:       5,
			Period:          10 * time.Second,
			RestartStrategy: OneForOne,
			Workers: []WorkerOptions{
				WorkerOptions{
					Name:            "Fail Worker",
					Action:          workerAction,
					CancelTimeout:   100 * time.Microsecond,
					RestartStrategy: Transient,
				},
			},
		},
		[]func(ProcessEvent) error{
			statusMatches("Test Supervisor", STARTED),
			statusMatches("Test Supervisor", CANCELED),
		},
	)
	// assert lifecycle is as expected
	if err != nil {
		t.Error(err)
	}
}

func TestSupervisorQuotaReached(t *testing.T) {
	defer func() {
		err, ok := recover().(error)
		if ok && err != nil {
			if !strings.HasPrefix(err.Error(), "Restart Quota Exceeded") {
				t.Errorf("Expecting to raise error around quota exceded, but didn't: %s", err.Error())
			}
		}
	}()

	var forever sync.WaitGroup
	workerAction := failTimesAndWait(3, func() { forever.Wait() })
	err := joinSupervisor(
		SupervisorOptions{
			Name:            "Test Supervisor",
			Intensity:       1,
			Period:          10 * time.Second,
			RestartStrategy: OneForOne,
			Workers: []WorkerOptions{
				WorkerOptions{
					Name:            "Fail Worker",
					Action:          workerAction,
					RestartStrategy: Transient,
				},
			},
		},
		[]func(ProcessEvent) error{
			statusMatches("Test Supervisor", STARTED),
			statusMatches("Fail Worker", STARTED),
		},
	)
	// assert lifecycle is as expected
	if err != nil {
		t.Error(err)
	}
}
