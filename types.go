package capataz

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/google/uuid"
)

///

// Identifier for Workers, used for tracing and canceling
type ProcessId uuid.UUID

func (p ProcessId) String() string {
	return uuid.UUID(p).String()
}

func newProcessId() ProcessId {
	return ProcessId(uuid.New())
}

///

// Number of times an error may happen under supervision
type Intensity int

///

// Different events that can be emitted from the supervision system
type ProcessEventTag int

const (
	STARTED ProcessEventTag = iota
	COMPLETED
	FAILED
	CANCELED
)

func (t ProcessEventTag) String() string {
	switch t {
	case STARTED:
		return "STARTED"
	case COMPLETED:
		return "COMPLETED"
	case FAILED:
		return "FAILED"
	case CANCELED:
		return "CANCELED"
	default:
		panic("This should never happen")

	}
}

// Wraps worker and supervisor goroutines in a common interface
//
// NOTE: this is going to become handy when Supervision Trees get supported
type Process interface {
	Id() ProcessId
	Name() string
	SpawnTime() time.Time
	RestartCount() int64
	RestartStrategy() ProcessRestartStrategyTag
	CancelTimeout() time.Duration
}

///

// Represents an event being triggered from the Supervision system for
// traceability purposes
//
type ProcessEvent interface {
	Process
	Error() error
	EventTime() time.Time
	Status() ProcessEventTag
}

// Concrete implementation of ProcessEvent
//
type processEvent struct {
	process   Process
	status    ProcessEventTag
	timestamp time.Time
	error     error
}

func (ev processEvent) Id() ProcessId {
	return ev.process.Id()
}

func (ev processEvent) Name() string {
	return ev.process.Name()
}

func (ev processEvent) SpawnTime() time.Time {
	return ev.process.SpawnTime()
}

func (ev processEvent) RestartCount() int64 {
	return ev.process.RestartCount()
}

func (ev processEvent) RestartStrategy() ProcessRestartStrategyTag {
	return ev.process.RestartStrategy()
}

func (ev processEvent) Error() error {
	return ev.error
}

func (ev processEvent) EventTime() time.Time {
	return ev.timestamp
}

func (ev processEvent) CancelTimeout() time.Duration {
	return ev.process.CancelTimeout()
}

var _ ProcessEvent = &processEvent{}

func (ev processEvent) Status() ProcessEventTag {
	return ev.status
}

// Used to send ProcessEvent records to callers of the Capataz API
type eventNotifier func(ev ProcessEvent)

func (n eventNotifier) notifyEvent(tag ProcessEventTag, err error, p Process) {
	n(processEvent{
		process:   p,
		status:    tag,
		timestamp: time.Now(),
		error:     err,
	})
}

func (n eventNotifier) NotifyStarted(p Process) {
	n.notifyEvent(STARTED, nil, p)
}

func (n eventNotifier) NotifyCompleted(p Process) {
	n.notifyEvent(COMPLETED, nil, p)
}

func (n eventNotifier) NotifyFailed(err error, p Process) {
	n.notifyEvent(FAILED, err, p)
}

func (n eventNotifier) NotifyCanceled(p Process) {
	n.notifyEvent(CANCELED, nil, p)
}

///

type monitorEventTag int

const (
	WorkerCompleted monitorEventTag = iota
	WorkerFailed
	WorkerStarted
	WorkerCanceled
	// SupervisorStarted
	// ProcessForcedRestart
)

func (t monitorEventTag) String() string {
	switch t {
	case WorkerCompleted:
		return "WorkerCompleted"
	case WorkerFailed:
		return "WorkerFailed"
	case WorkerStarted:
		return "WorkerStarted"
	case WorkerCanceled:
		return "WorkerCanceled"
	default:
		panic("Invalid monitorEventTag")
	}
}

type workerFailed struct {
	worker worker
	err    error
}

type workerStarted struct {
	worker     worker
	cancelFunc context.CancelFunc
}

// Represents an event that comes from supervised worker goroutines.
//
// NOTE: This struct represents a union type that will contain one of the
// attributes at a time depending on the tag (poor mans algebraic data type).
//
// TODO: Revisit this ADT model with a Visitor pattern instead
type monitorEvent struct {
	tag             monitorEventTag
	workerStarted   *workerStarted
	workerCompleted *worker
	workerCanceled  *worker
	workerFailed    *workerFailed
}

// Used to send monitorEvent records to the Supervisor thread
//
// NOTE: This API is identical to the ProcessEvent API, in the future we maybe
// want to unify the two; still need to figure if it is ok for them to be
// coupled.
type monitorNotifier func(ev monitorEvent)

func (m monitorNotifier) NotifyStarted(cancelFunc context.CancelFunc, w worker) (succeed bool) {
	defer func() {
		if r := recover(); r != nil {
			succeed = false
		}
	}()
	m(monitorEvent{
		tag:             WorkerStarted,
		workerFailed:    nil,
		workerStarted:   &workerStarted{worker: w, cancelFunc: cancelFunc},
		workerCompleted: nil,
	})
	return true
}

func (m monitorNotifier) NotifyCompleted(w worker) {
	// NOTE: Notify functions fail on emition when supervisor channel gets
	// closed, this happens when shutting down a supervisor and the spawn of
	// a new worker happened just before the stop (race-condition)
	//
	// This implementation is making assumptions about the underlying
	// transport (channels), so this might need to change when other
	// notification transports are implemented (if it ever comes to that)
	defer func() {
		_ = recover()
	}()
	m(monitorEvent{
		tag:             WorkerCompleted,
		workerFailed:    nil,
		workerStarted:   nil,
		workerCompleted: &w,
	})
}

func (m monitorNotifier) NotifyFailed(w worker, err error) {
	// NOTE: Notify functions fail on emition when supervisor channel gets
	// closed, this happens when shutting down a supervisor and the spawn of
	// a new worker happened just before the stop (race-condition)
	//
	// This implementation is making assumptions about the underlying
	// transport (channels), so this might need to change when other
	// notification transports are implemented (if it ever comes to that)
	defer func() {
		_ = recover()
	}()
	m(monitorEvent{
		tag:             WorkerFailed,
		workerFailed:    &workerFailed{worker: w, err: err},
		workerStarted:   nil,
		workerCompleted: nil,
	})
}

func (m monitorNotifier) NotifyCanceled(w worker) {
	// NOTE: Notify functions fail on emition when supervisor channel gets
	// closed, this happens when shutting down a supervisor and the spawn of
	// a new worker happened just before the stop (race-condition)
	//
	// This implementation is making assumptions about the underlying
	// transport (channels), so this might need to change when other
	// notification transports are implemented (if it ever comes to that)
	defer func() {
		_ = recover()
	}()
	m(monitorEvent{
		tag:             WorkerCanceled,
		workerCanceled:  &w,
		workerFailed:    nil,
		workerStarted:   nil,
		workerCompleted: nil,
	})
}

///

type controlMessageTag int

// Represents a message that comes from Capataz Public API
//
// NOTE: This struct represents a union type that will contain one of the
// attributes at a time depending on the tag (poor mans algebraic data type).
//
// TODO: Revisit this ADT model with a Visitor pattern instead
type controlMessage struct {
	tag          controlMessageTag
	forkWorker   *worker
	cancelWorker *ProcessId
}

const (
	forkWorker controlMessageTag = iota
	cancelWorker
	// forkSupervisor
)

// Used to send controlMessage records to the Supervisor thread from
// the public API.
//
type controlNotifier func(msg controlMessage)

///

// Specification on when a failing process should be restarted.
type ProcessRestartStrategyTag int

const (
	// Only restart if the goroutine failed
	Transient ProcessRestartStrategyTag = iota
	// Always restart even if the goroutine finished without errors
	Permanent
	// Never restart, even if the goroutine failed
	Temporary
)

func (t ProcessRestartStrategyTag) String() string {
	switch t {
	case Transient:
		return "TRANSIENT"
	case Permanent:
		return "PERMANENT"
	case Temporary:
		return "TEMPORARY"
	default:
		panic("Invalid ProcessRestartStrategyTag")
	}
}

// Specification for a Worker goroutine, it has a name for traceability
// purposes, the actual function that is going to be executing in a goroutine,
// the amount of time it can be exected (optional), and a RestartStrategy to
// address restart logic in case the goroutine fails
//
type WorkerOptions struct {
	// Name of the worker gorutine, used for traceability purposes
	Name string
	// Function invoked every time a worker fails, you can return a duration
	// that will be used to delay the restart, excellent for exponential
	// backoff strategies
	BackoffOnError func(err error, restartCount int64) (retryDelay time.Duration)
	// How much time a supervisor should be willing to wait for this worker
	// goroutine to get back to it after a cancel.
	//
	// If timeout is not respected, the supervisor will kill all supervised workers
	// and report an error to the upper layer
	//
	// Defaults to 500 milliseconds if not specified.
	CancelTimeout time.Duration
	// Duration which the worker can execute, defaults to 0 (run forever)
	Timeout time.Duration
	// Specifies if the worker should always be restarted, or only restarted
	// on error, or not being restarted at all. See ProcessRestartStrategyTag
	RestartStrategy ProcessRestartStrategyTag
	// Function that is going to be executed in a new goroutine (the
	// function you would pass to your go statement)
	Action func(context.Context) error
}

// runtime representation of WorkerOptions
type worker struct {
	id           ProcessId
	restartCount int64
	options      WorkerOptions
	spawnTime    time.Time
	onStart      func()
	onCompleted  func()
	onError      func(error)
}

func (w worker) String() string {
	return fmt.Sprintf(
		"Worker{id: %s, name: %s, restartCount: %d}",
		w.id,
		w.options.Name,
		w.restartCount,
	)
}

func (w worker) Id() ProcessId {
	return w.id
}

func (w worker) SpawnTime() time.Time {
	return w.spawnTime
}

func (w worker) RestartCount() int64 {
	return w.restartCount
}

func (w worker) Name() string {
	return w.options.Name
}

func (w worker) RestartStrategy() ProcessRestartStrategyTag {
	return w.options.RestartStrategy
}

func (w worker) CancelTimeout() time.Duration {
	return w.options.CancelTimeout
}

var _ Process = &worker{}

///

type SupervisorTerminationOrder int

const (
	// Restart or Cancel workers on a supervisor from left to right (normal order)
	LeftToRight SupervisorTerminationOrder = iota
	// Restart or Cancel workers on a supervisor from right to left (reversed order)
	RightToLeft
)

func (o SupervisorTerminationOrder) String() string {
	switch o {
	case LeftToRight:
		return "LeftToRight"
	case RightToLeft:
		return "RightToLeft"
	default:
		panic("Invalid SupervisorTerminationOrder")
	}
}

// Specification on how the supervisor should restart workers in combination
// with it's siblings.
type SupervisorRestartStrategyTag int

const (
	// Supervisor restarts the failing worker goroutine only
	OneForOne SupervisorRestartStrategyTag = iota
	// Supervisor restarts all the worker goroutines when one fails
	AllForOne
)

func (t SupervisorRestartStrategyTag) String() string {
	switch t {
	case OneForOne:
		return "OneForOne"
	case AllForOne:
		return "AllForOne"
	default:
		panic("Invalid SupervisorRestartStrategyTag")
	}
}

// Specification for Supervisors given by a caller of the API
type SupervisorOptions struct {
	// Name of the supervisor gorutine, used for traceability purposes
	Name string
	// Number of errors that a supervisor is able to tolerate (used in comibination with the Period attribute). It must be greater than 0, otherwise the program will panic
	Intensity int
	// Window of time where the supervisor can tolerate Intensity amount of errors (used in combination with the Intensity attribute). It must be greater than 0, otherwise the program will panic
	Period time.Duration
	// Buffer size for channels used internally in the supervisor.
	BufferSize int
	// Specifies how the supervised workers should terminate. If
	// LeftToRight, it cancels the supervised workers in the given order; if
	// RightToLeft, it cancels the supervised workers in reversed order.
	TerminationOrder SupervisorTerminationOrder
	// If true, it will catch panic errors and handle them through the
	// Supervisor restart mechanisms.
	//
	// Defaults to false.
	RescuePanic bool
	// Specifies the behavior when one supervised worker fails. If
	// OneForOne, it will restart only the failing worker; if AllForOne, it
	// will cancel all supervised workers and only restart the ones that
	// were specified in the Workers attribute.
	//
	// WARNING: If you use ForkWorker, those workers _are not_ going to be
	// restarted when using an AllForOne strategy.
	RestartStrategy SupervisorRestartStrategyTag
	// List of Workers that you want to get started as soon as the
	// Supervisor starts. They will also guaranteed to be restarted when
	// using an AllForOne restart strategy.
	Workers []WorkerOptions
}

// Normalized specification for Supervisors, whenever we have an instance of
// this record, all attributes must be valid.
type supervisorOptions struct {
	// Name of the supervisor gorutine, used for traceability purposes
	Name string
	// Number of errors that a supervisor is able to tolerate (used in comibination with the Period attribute). It must be greater than 0, otherwise the program will panic
	Intensity Intensity
	// Window of time where the supervisor can tolerate Intensity amount of errors (used in combination with the Intensity attribute). It must be greater than 0, otherwise the program will panic
	Period time.Duration
	// Buffer size for channels used internally in the supervisor.
	BufferSize int
	// Specifies the behavior when one supervised worker fails. If
	// OneForOne, it will restart only the failing worker; if AllForOne, it
	// will cancel all supervised workers and only restart the ones that
	// were specified in the Workers attribute.
	//
	// WARNING: If you use ForkWorker, those workers _are not_ going to be
	// restarted when using an AllForOne strategy.
	RestartStrategy SupervisorRestartStrategyTag
	// Specifies how the supervised workers should terminate. If
	// LeftToRight, it cancels the supervised workers in the given order; if
	// RightToLeft, it cancels the supervised workers in reversed order.
	TerminationOrder SupervisorTerminationOrder
	// If true, it will catch panic errors and handle them through the
	// Supervisor restart mechanisms.
	//
	// Defaults to false.
	RescuePanic bool
	// List of Workers that you want to get started as soon as the
	// Supervisor starts. They will also guaranteed to be restarted when
	// using an AllForOne restart strategy.
	Workers []WorkerOptions
	// OnIntensityReached func()
}

// Lifecyle of a Supervisor
type ProcessStatusTag int

const (
	INIT ProcessStatusTag = iota
	RUNNING
	RESTARTING
	HALTING
	FAILING
)

func (t ProcessStatusTag) String() string {
	switch t {
	case INIT:
		return "INIT"
	case RUNNING:
		return "RUNNING"
	case RESTARTING:
		return "RESTARTING"
	case HALTING:
		return "HALTING"
	case FAILING:
		return "FAILING"
	default:
		panic("Unknwown Proces Status Tag")
	}
}

// Record that contains all metadata of the Supervisor goroutine, this record
// may be used to cancel workers supervised by it.
type Supervisor struct {
	id        ProcessId
	spawnTime time.Time
	Options   supervisorOptions
	// This field is not thread safe, as we must use it only
	// on the supervisor goroutine
	joinAction       func()
	workerRuntimeMap map[ProcessId]context.CancelFunc
	workerSpecs      []worker
	sendControl      controlNotifier
	monitorNotifier  monitorNotifier
	eventNotifier    eventNotifier
	workerWaitGroup  *sync.WaitGroup
}

func (s Supervisor) Id() ProcessId {
	return s.id
}

func (s Supervisor) SpawnTime() time.Time {
	return s.spawnTime
}

func (s Supervisor) RestartCount() int64 {
	return 0
}

func (s Supervisor) Name() string {
	return s.Options.Name
}

func (_ Supervisor) RestartStrategy() ProcessRestartStrategyTag {
	return Permanent
}

func (_ Supervisor) CancelTimeout() time.Duration {
	return -1
}

var _ Process = &Supervisor{}
