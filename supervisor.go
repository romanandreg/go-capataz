package capataz

import (
	"context"
	"fmt"
	"sync"
	"time"
)

// Creates a new gorutine that can supervise other worker goroutines using
// high-level semantics (OTP style) from the given SupervisorOptions
func NewSupervisor(opts SupervisorOptions) (Supervisor, context.CancelFunc) {
	return NewSupervisor1(nil, opts)
}

// Creates a new goroutine that can supervise other worker goroutines using
// high-level semantics (OTP style) from the given SupervisorOptions. It
// receives a channel where the supervisor emits events that happen inside the
// supervisor, this may be used for logging and monitoring
func NewSupervisor1(broadcastChan chan<- ProcessEvent, input SupervisorOptions) (Supervisor, context.CancelFunc) {
	supervisorId := newProcessId()
	opts := validateSupervisorOpts(input)
	spawnTime := time.Now()

	monitorChan := make(chan monitorEvent)
	controlChan := make(chan controlMessage)

	rootCtx, rootCancel := context.WithCancel(context.Background())

	workerSpecs := buildStaticWorkers(opts.Workers)
	monitorNotifier := buildMonitorNotifier(monitorChan)
	controlNotifier := buildControlNotifier(controlChan)
	eventNotifier := buildEventNotifier(broadcastChan)
	exitChan := make(chan error, 1)

	var workerWg sync.WaitGroup
	supervisor := Supervisor{
		Options:          opts,
		id:               supervisorId,
		spawnTime:        spawnTime,
		workerRuntimeMap: make(map[ProcessId]context.CancelFunc),
		workerSpecs:      workerSpecs,
		monitorNotifier:  monitorNotifier,
		eventNotifier:    eventNotifier,
		sendControl:      controlNotifier,
		workerWaitGroup:  &workerWg,
		joinAction: func() {
			err, ok := <-exitChan
			if !ok {
				panic("Invalid state reached")
			}
			panic(err)
		},
	}

	// Start supervisor thread
	go func() {
		eventNotifier.NotifyStarted(supervisor)
		err := supervisor.runMainLoop(
			rootCtx,
			INIT,
			monitorChan,
			controlChan,
		)
		close(monitorChan)
		close(controlChan)
		if err != nil {
			eventNotifier.NotifyFailed(err, supervisor)
			exitChan <- err
		} else {
			eventNotifier.NotifyCanceled(supervisor)
			exitChan <- nil
		}
	}()

	stopSupervisor := context.CancelFunc(func() {
		rootCancel()
		workerWg.Wait()
		err, ok := <-exitChan
		if ok && err != nil {
			panic(err)
		}
	})

	return supervisor, stopSupervisor
}

// Normalizes the given input of a SupervisorOptions, in case the input is
// invalid it panics. This error mechanism was chosen over returning an error
// given this (in an ideal scenario) should be a compilation error.
func validateSupervisorOpts(input SupervisorOptions) supervisorOptions {
	if input.Name == "" {
		panic(fmt.Sprintf("Received supervisor options without a name"))
	}
	if input.Intensity <= 0 {
		panic(fmt.Sprintf("Received supervisor options with non-positive intensity: %d", input.Intensity))
	}
	if input.Period <= 0 {
		panic(fmt.Sprintf("Received supervisor options with non-positive period: %d", input.Period))
	}
	if input.BufferSize < 0 {
		panic(fmt.Sprintf("Received supervisor options with non-positive buffer size: %d", input.BufferSize))
	}

	return supervisorOptions{
		Name:             input.Name,
		Intensity:        Intensity(input.Intensity),
		BufferSize:       input.BufferSize,
		Period:           input.Period,
		RescuePanic:      input.RescuePanic,
		RestartStrategy:  input.RestartStrategy,
		TerminationOrder: input.TerminationOrder,
		Workers:          input.Workers,
	}
}

// Hides implementation details around how the monitor event notifier is
// implemented (using channels)
func buildMonitorNotifier(monitorChan chan<- monitorEvent) monitorNotifier {
	return monitorNotifier(func(ev monitorEvent) {
		monitorChan <- ev
	})
}

// Hides implementation details around how the control message notifier is
// implemented (using channels)
func buildControlNotifier(controlChan chan<- controlMessage) controlNotifier {
	return controlNotifier(func(msg controlMessage) {
		controlChan <- msg
	})
}

// Hides implementation details around how the event notification is
// implemented (using channels)
func buildEventNotifier(eventChan chan<- ProcessEvent) eventNotifier {
	if eventChan == nil {
		return func(_ev ProcessEvent) {}
	} else {
		return eventNotifier(func(ev ProcessEvent) {
			eventChan <- ev
		})

	}
}

func buildStaticWorkers(workersOpts []WorkerOptions) []worker {
	workers := make([]worker, len(workersOpts))
	for i, workerOpts := range workersOpts {
		workers[i] = buildWorker(workerOpts)
	}
	return workers
}


func (s Supervisor) waitForProcessStartedConfirmation(rootCtx context.Context, p Process, monitorChan chan monitorEvent) {
	select {
	case <-rootCtx.Done():
		// abort if we are done
	case ev, ok := <-monitorChan:
		if ok {
			switch ev.tag {
			case WorkerStarted:
				worker := ev.workerStarted.worker
				if worker.Id() == p.Id() {
					// NOTE: no need to keep track
					// of the supervisor status on
					// this sub-state machine as we
					// are filtering very specific
					// messages
					_, _ = s.handleWorkerStarted(*ev.workerStarted)
				} else {
					panic("INVARIANT VIOLATED, there should be one worker starting at a time")
				}
			default:
				panic("INVARIANT VIOLATED, there should be one worker starting at a time")
			}
		}
	}
}

// Transform all the workers specified in the SupervisorOptions into goroutines
func (s Supervisor) spawnStaticWorkers(rootCtx context.Context, monitorChan chan monitorEvent) {
	for _, worker := range s.workerSpecs {
		doForkWorker(rootCtx, s.Options.RescuePanic, s.monitorNotifier, s.workerWaitGroup, worker)
		s.waitForProcessStartedConfirmation(rootCtx, worker, monitorChan)
	}

}

/*** Supervisor Loop ***/

func buildWorker(opts WorkerOptions) worker {
	if opts.Name == "" {
		panic("Received worker options with empty name")
	}
	if opts.CancelTimeout == 0 {
		// INVESTIGATE: ways to discover a struct attribute has a zero
		// value (rather than explicitly provided by client)
		//
		// Assuming zero value, offer default value of 500 millis
		opts.CancelTimeout = 500 * time.Millisecond
	} else if opts.CancelTimeout < 0 {
		panic(fmt.Sprintf("Received worker options with negative cancel timeout: %d", opts.CancelTimeout))
	}
	workerId := newProcessId()
	worker := worker{
		id:           workerId,
		options:      opts,
		restartCount: 0,
		spawnTime:    time.Now(),
		onStart:      func() {},
		onCompleted:  func() {},
		onError:      func(_ error) {},
	}
	return worker

}

// Forks a new Worker goroutine with the given WorkerOptions on this supervisor;
// it returns a ProcessId that may be used to stop it later on
func (s Supervisor) ForkWorker(opts WorkerOptions) ProcessId {
	worker := buildWorker(opts)
	s.sendControl(controlMessage{tag: forkWorker, forkWorker: &worker, cancelWorker: nil})
	return worker.Id()
}

// Stops a Worker goroutine that was spawned on the supervision tree, you have to spawn
// one first using ForkWorker
func (s Supervisor) CancelWorker(pid ProcessId) {
	s.sendControl(controlMessage{tag: cancelWorker, forkWorker: nil, cancelWorker: &pid})
}

func (s Supervisor) Join() {
	s.joinAction()
}

// Forks a new supervised worker, sends notifications to the supervisor when the
// goroutine starts, fails, gets canceled or completes without errors; it also
// creates a local context to cancel this worker only.
func doForkWorker(
	rootCtx context.Context,
	rescuePanic bool,
	notifier monitorNotifier,
	workerWaitGroup *sync.WaitGroup,
	w worker,
) {
	// This worker group keeps track of the workers spawned so far, this way
	// we don't finish the supervisor without being sure all worker
	// goroutines are done
	workerWaitGroup.Add(1)

	// Given we have this algorithm in both panic and normal errors, let's
	// have it in its own function to not duplicate work
	onError := func(err error) {
		if w.options.BackoffOnError != nil {
			time.Sleep(w.options.BackoffOnError(err, w.restartCount))
		}
		w.onError(err)
		notifier.NotifyFailed(w, err)

	}
	w.spawnTime = time.Now()
	go func() {
		// NOTE: Keep the defer order as is; we want to notify the
		// supervisor _before_ calling Done on the worker wait group
		defer func() {
			workerWaitGroup.Done()
		}()
		if rescuePanic {
			defer func() {
				// Swallow panic on workers and register them as
				// errors in the supervisor
				var err error
				var ok bool
				if err0 := recover(); err0 != nil {
					err, ok = err0.(error)
					if !ok {
						err = fmt.Errorf("panic: %s", err0.(string))
					}
					onError(err)
				}
			}()
		}
		if rootCtx.Err() == nil {
			ctx, cancelFn := context.WithCancel(context.Background())
			defer cancelFn()
			// NOTE: NotifyStarted function returns false when
			// supervisor channel gets closed, this happens when
			// shutting down a supervisor and the spawn of a new
			// worker happened just before the stop (race-condition)
			ok := notifier.NotifyStarted(cancelFn, w)

			// We want to notify the supervisor _after_ callbacks
			// in the worker goroutine are done, except for onStart as
			// we want to halt as soon as we know the supervisor is
			// no longer receiving messages
			if ok {
				w.onStart()
				err := w.options.Action(ctx)
				if err != nil {
					if err != context.Canceled {
						onError(err)
					} else {
						w.onError(err)
						notifier.NotifyCanceled(w)
					}
				} else {
					w.onCompleted()
					notifier.NotifyCompleted(w)
				}
			}
		}
	}()
}

// Removes the worker from the supervisor's runtime map and also cancels the
// local worker context.
//
// TODO: make sure this function is general purpose when we support supervision
// trees
func (s Supervisor) doCancelWorker(pid ProcessId) {
	cancelWorker := s.workerRuntimeMap[pid]
	delete(s.workerRuntimeMap, pid)
	cancelWorker()
}

func (s Supervisor) handleControlMessage(
	rootCtx context.Context,
	msg controlMessage,
) (nextStatus ProcessStatusTag, err error) {
	switch msg.tag {
	case forkWorker:
		doForkWorker(rootCtx, s.Options.RescuePanic, s.monitorNotifier, s.workerWaitGroup, *msg.forkWorker)
		return RUNNING, nil
	case cancelWorker:
		s.doCancelWorker(*msg.cancelWorker)
		return RUNNING, nil
	default:
		return FAILING, fmt.Errorf("handleControlMessage: unknown control message tag")
	}
}

/* Supervisor Monitor Handler */

func (s Supervisor) execSupervisorRestartStrategy(
	rootCtx context.Context,
	monitorChan chan monitorEvent, /* needed for AllForOne */
	w worker,
) (nextStatus ProcessStatusTag, err error) {
	switch s.Options.RestartStrategy {
	case OneForOne:
		doForkWorker(rootCtx, s.Options.RescuePanic, s.monitorNotifier, s.workerWaitGroup, w)
		return RUNNING, nil
	case AllForOne:
		pendingEvents, err := s.cancelAllWorkers(rootCtx, monitorChan)
		if err != nil {
			return FAILING, err
		} else {
			for _, ev := range pendingEvents {
				monitorChan <- ev
			}
			s.spawnStaticWorkers(rootCtx, monitorChan)
			return RUNNING, nil
		}
	default:
		return FAILING, fmt.Errorf("execSupervisorRestartStrategy: unknown restart strategy tag")
	}
}

// ranting strongly. like. really. strongly Specially because there is nothing
// 'worker' specific in this function what tha fudge type system? Get your
// shtuff together...
func cantBelieveIHaveToCodeReverseMyself(ws []worker) []worker {
	for i, j := 0, len(ws)-1; i < j; i, j = i+1, j-1 {
		ws[i], ws[j] = ws[j], ws[i]
	}
	return ws
}

func (s Supervisor) waitForProcessCancelConfirmation(
	rootCtx context.Context,
	pendingEvents *[]monitorEvent,
	p Process,
	monitorChan chan monitorEvent,
) error {

	handleCancelEvent := func(ev monitorEvent, ok bool) {
		if ok {
			switch ev.tag {
			case WorkerCanceled:
				worker := ev.workerCanceled
				if worker.Id() == p.Id() {
					// NOTE: no need to keep track
					// of the supervisor status on
					// this sub-state machine as we
					// are filtering very specific
					// messages
					_, _ = s.handleWorkerCanceled(*worker)
					return
				} else {
					*pendingEvents = append(*pendingEvents, ev)
				}
			case WorkerCompleted:
				worker := ev.workerCompleted
				if worker.Id() == p.Id() {
					// NOTE: diito from comment on previous block
					_, _ = s.handleWorkerCompleted(rootCtx, monitorChan, HALTING, *worker)
					return
				} else {
					*pendingEvents = append(*pendingEvents, ev)
				}
			case WorkerFailed:
				f := ev.workerFailed
				if f.worker.Id() == p.Id() {
					// NOTE: diito from comment on previous block
					_, _ = s.handleWorkerFailed(rootCtx, monitorChan, HALTING, *f)
				} else {
					*pendingEvents = append(*pendingEvents, ev)
				}
			default:
				*pendingEvents = append(*pendingEvents, ev)
			}
		}
	}
	for {
		if p.CancelTimeout() > 0 {
			select {
			case ev, ok := <-monitorChan:
				handleCancelEvent(ev, ok)
				return nil
			case <-time.After(p.CancelTimeout()):
				return fmt.Errorf("Process %s with name '%s' didn't terminate on cancel after timeout",
					p.Id().String(),
					p.Name())
			}
		} else {
			ev, ok := <-monitorChan
			handleCancelEvent(ev, ok)
			return nil
		}

	}
}

// Blocks the supervisor waiting for all cancel events from workers in the
// specified order; this procedure guarantees that the termination order of the
// workers is stable. It returns a slice with all the monitor events that were
// not related to the expected stop event from the expected worker
func (s Supervisor) cancelAllWorkers(
	rootCtx context.Context,
	monitorChan chan monitorEvent,
) (nonRelatedEvents []monitorEvent, err error) {
	var workers *[]worker

	// Order workers in termination order
	switch s.Options.TerminationOrder {
	case LeftToRight:
		workers = &s.workerSpecs
	case RightToLeft:
		// https://stackoverflow.com/questions/10535743/address-of-a-temporary-in-go
		tmp := cantBelieveIHaveToCodeReverseMyself(s.workerSpecs)
		workers = &tmp
	default:
		panic("cancelAllWorkers: Invalid SupervisorTerminationOrder")
	}

	// Create a temporal buffer where all events that are not related to
	// the cancel of the current worker are stored
	var pendingEvents []monitorEvent

	// NOTE: Was considering doing a reduce here instead of a for loop with
	// side effect/mutation on the pendingEvents slice, it seems authors
	// of the language promote mutation over safe composition:
	//
	// https://stackoverflow.com/a/49469339
	for _, worker := range *workers {
		workerCancelFn, ok := s.workerRuntimeMap[worker.Id()]
		if ok {
			workerCancelFn()
			err = s.waitForProcessCancelConfirmation(rootCtx, &pendingEvents, worker, monitorChan)
		}
	}
	return pendingEvents, err
}

func verifyRestartQuota(intensity Intensity, period time.Duration, w *worker) (*worker, bool) {
	// we increment the restartCount as we are currently in a restart operation
	w.restartCount = w.restartCount + 1
	// the worker restart count has reached intensity in the given period of time, so we should abort
	if w.restartCount > int64(intensity) && time.Since(w.spawnTime) < period {
		return w, true
	}
	if time.Since(w.spawnTime) > period {
		// Enough time has passed to allow a new error to happen, we need to update
		// the restart information of worker to make sure it doesn't reach the restart
		// quota in this new window of time.
		w.spawnTime = time.Now()

		// We execute this function while doing a restart, ergo, we need
		// to account for the restart happening now
		w.restartCount = 1
	}
	return w, false
}

func (s Supervisor) handleWorkerStarted(ws workerStarted) (nextStatus ProcessStatusTag, err error) {
	s.workerRuntimeMap[ws.worker.Id()] = ws.cancelFunc
	s.eventNotifier.NotifyStarted(ws.worker)
	return RUNNING, nil
}

func (s Supervisor) handleWorkerFailed(
	rootCtx context.Context,
	monitorChan chan monitorEvent, /* needed for AllForOne */
	status ProcessStatusTag,
	f workerFailed,
) (nextStatus ProcessStatusTag, err error) {
	delete(s.workerRuntimeMap, f.worker.Id())
	s.eventNotifier.NotifyFailed(f.err, f.worker)
	switch f.worker.options.RestartStrategy {
	case Transient, Permanent:
		updatedWorker, shouldAbort := verifyRestartQuota(s.Options.Intensity, s.Options.Period, &f.worker)
		if shouldAbort {
			// TODO: Support supervision trees by notifying my parent supervisor here
			return FAILING, fmt.Errorf("Restart Quota Exceeded for worker: %s", updatedWorker.String())
		} else if status == RUNNING {
			return s.execSupervisorRestartStrategy(rootCtx, monitorChan, *updatedWorker)
		} else {
			return status, nil
		}
	case Temporary:
		return status, nil
	default:
		return FAILING, fmt.Errorf("handleWorkerFailed: unknown worker restart strategy tag")
	}
}

func (s Supervisor) handleWorkerCompleted(
	rootCtx context.Context,
	monitorChan chan monitorEvent, /* needed for AllForOne */
	status ProcessStatusTag,
	w worker,
) (nextStatus ProcessStatusTag, err error) {
	delete(s.workerRuntimeMap, w.Id())
	s.eventNotifier.NotifyCompleted(w)
	switch w.options.RestartStrategy {
	case Permanent:
		w.restartCount = 0
		w.spawnTime = time.Now()
		if status == RUNNING {
			return s.execSupervisorRestartStrategy(rootCtx, monitorChan, w)
		} else {
			return status, nil
		}
	case Transient, Temporary:
		return status, nil
	default:
		return status, nil
	}
}

func (s Supervisor) handleWorkerCanceled(w worker) (nextStatus ProcessStatusTag, err error) {
	delete(s.workerRuntimeMap, w.Id())
	s.eventNotifier.NotifyCanceled(w)
	return RUNNING, nil
}

// Handles events sent from supervised workers, depending on the worker and
// supervisor options, a handling from this state can modify the current status
// of the supervisor, ergo, that's why we pass the supervisorContext and the
// current status
func (s Supervisor) handleMonitorEvent(
	rootCtx context.Context,
	monitorChan chan monitorEvent, /* needed for AllForOne */
	status ProcessStatusTag,
	ev monitorEvent,
) (nextStatus ProcessStatusTag, err error) {
	switch ev.tag {
	case WorkerStarted:
		return s.handleWorkerStarted(*ev.workerStarted)
	case WorkerCompleted:
		return s.handleWorkerCompleted(rootCtx, monitorChan, status, *ev.workerCompleted)
	case WorkerFailed:
		return s.handleWorkerFailed(rootCtx, monitorChan, status, *ev.workerFailed)
	case WorkerCanceled:
		return s.handleWorkerCanceled(*ev.workerCanceled)
	default:
		return FAILING, fmt.Errorf("handleWorkerFailed: unknown monitor event tag")
	}
}

// Executes the supervisor thread, this thread is responsible of detecting
// errors on other goroutines and restart them appropietly given a set of
// settings.
func (s Supervisor) runMainLoop(
	rootCtx context.Context,
	status ProcessStatusTag,
	monitorChan chan monitorEvent,
	controlChan <-chan controlMessage,
) error {
	var moreEv, moreMsg bool
	var ev monitorEvent
	var msg controlMessage
	var err error

	// The supervisor will loop on different status;

	for {
		switch status {
		case INIT:
			// Start sending to monitor channel the initialization of workers
			s.spawnStaticWorkers(rootCtx, monitorChan)
			status = RUNNING


		case FAILING:
			// This case happens when a failure quota was reached,
			// or an implementation error is discovered at runtime
			// (e.g. bad case statement), our procedure the code
			// that moved us to the FAILING status should have had
			// stopped the context of all workers of this
			// supervisor; we should just wait for all of them to
			// die and then exit with an error
			_, _ = s.cancelAllWorkers(rootCtx, monitorChan)
			return err

		case RUNNING:
			// This case is regular supervisor behavior, we wait
			// for messages from control (client API), monitor (workers) or the
			// root context

			select {
			case <-rootCtx.Done():
				_, err := s.cancelAllWorkers(rootCtx, monitorChan)
				return err

			case msg, moreMsg = <-controlChan:
				if moreMsg {
					status, err = s.handleControlMessage(rootCtx, msg)
				}

			case ev, moreEv = <-monitorChan:
				if moreEv {
					status, err = s.handleMonitorEvent(rootCtx, monitorChan, status, ev)
				}

			}
			if !moreMsg || !moreEv {
				break
			}
		default:
			return fmt.Errorf("Unknown ProcessStatusTag value")
		}
	}
}
