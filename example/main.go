package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	c ".."
	"github.com/apex/log"
	lh "github.com/apex/log/handlers/json"
)

/**** main ****/

func echoPathHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
}

func main() {
	log.SetHandler(lh.New(os.Stdout))
	if true {
		staticSupervisor()

	} else {
		dynamicSupervisor()
	}
}

// An example where we specify workers on the SupervisorOptions so that they can
// get started everytime there is a restart via an AllForOne SupervisorRestartStrategy
func staticSupervisor() {
	http.HandleFunc("/", echoPathHandler)
	server := &http.Server{Addr: ":3000", Handler: nil}

	evChan := make(chan c.ProcessEvent)
	go func() {
		for ev := range evChan {
			fmt.Printf("[%s] %10s %s\n", ev.Id().String(), ev.Status(), ev.Name())
			if ev.Status() == c.FAILED {
				fmt.Printf("> ERROR: %v\n", ev.Error())
			}
			// log.WithFields(log.Fields{
			//	"Id":           ev.Id().String(),
			//	"ProcessName":  ev.Name(),
			//	"Status":       ev.Status().String(),
			//	"RestartCount": ev.RestartCount(),
			//	"SpawnTime":    ev.SpawnTime(),
			//	"EventTime":    ev.EventTime(),
			//	"Error":        ev.Error(),
			// }).Info("")
		}
	}()
	_, stopSup := c.NewSupervisor1(evChan, c.SupervisorOptions{
		Name:            "Root Supervisor",
		BufferSize:      1000,
		Intensity:       10,
		Period:          10 * time.Second,
		RestartStrategy: c.OneForOne,
		Workers: []c.WorkerOptions{
			c.WorkerOptions{
				Name: "My Worker",
				Action: func(_ context.Context) error {
					time.Sleep(500 * time.Millisecond)
					fmt.Println("Hello from My Worker")
					return nil
				},
				RestartStrategy: c.Permanent,
			},
			c.WorkerOptions{
				Name: "My Faulty Worker",
				Action: func(ctx context.Context) error {
					// delay to not reach the error intensity for supervisor
					select {
					case <-ctx.Done():
						return ctx.Err()
					case <-time.After(8000 * time.Millisecond):
						// NOTE: ^^^ play around with this timeout to see restarts happening
						return fmt.Errorf("This shit is realz")
					}
				},
				RestartStrategy: c.Transient,
			},
			c.WorkerOptions{
				Name:    "My Slow Worker",
				Timeout: 1 * time.Second,
				Action: func(ctx context.Context) error {
					for {
						select {
						case <-ctx.Done():
							return ctx.Err()
						case <-time.After(10 * time.Second):
							fmt.Println("Finished my slow action")
							return nil
						}
					}
				},
				RestartStrategy: c.Temporary,
			},
			c.WorkerOptions{
				Name: "My HTTP Server",
				Action: func(_ context.Context) error {
					err := server.ListenAndServe()
					return err
				},
				CancelTimeout:   1 * time.Second,
				RestartStrategy: c.Permanent,
			},
			c.WorkerOptions{
				Name: "My HTTP Server Shutdown",
				Action: func(ctx context.Context) error {
					<-ctx.Done()
					if err := server.Shutdown(ctx); err != nil {
						return err
					} else {
						return ctx.Err()
					}
				},
				RestartStrategy: c.Transient,
			},
		},
	})
	time.Sleep(3 * time.Second)
	fmt.Println("Terminating this program")
	stopSup()
	close(evChan)
	fmt.Println("End of program")

}

// An example where we generate workers on the fly
func dynamicSupervisor() {
	sup, stopSup := c.NewSupervisor(c.SupervisorOptions{
		Name:       "Root Supervisor",
		BufferSize: 1000,
		Intensity:  10,
		Period:     10 * time.Second,
	})

	// (1) task example with permanent restart
	sup.ForkWorker(c.WorkerOptions{
		Name: "My Worker",
		Action: func(_ context.Context) error {
			time.Sleep(500 * time.Millisecond)
			fmt.Println("Hello from Worker")
			return nil
		},
		RestartStrategy: c.Permanent,
	})

	// (2) faulty example with transient restart
	// faulty :=
	_ =
		sup.ForkWorker(c.WorkerOptions{
			Name: "My Faulty Worker",
			Action: func(ctx context.Context) error {
				// delay to not reach the error intensity for supervisor
				select {
				case <-ctx.Done():
					return ctx.Err()
				case <-time.After(8000 * time.Millisecond):
					return fmt.Errorf("This shit is realz")
				}
			},
			RestartStrategy: c.Transient,
		})

	// (3) timeout example
	sup.ForkWorker(c.WorkerOptions{
		Name:    "My Slow Worker",
		Timeout: 1 * time.Second,
		Action: func(ctx context.Context) error {
			for {
				select {
				case <-ctx.Done():
					return ctx.Err()
				case <-time.After(10 * time.Second):
					fmt.Println("Finished my slow action")
					return nil
				}
			}
		},
		RestartStrategy: c.Temporary,
	})

	// (4) http server in a worker
	// https://gist.github.com/peterhellberg/38117e546c217960747aacf689af3dc2

	http.HandleFunc("/", echoPathHandler)
	server := &http.Server{Addr: ":3000", Handler: nil}

	sup.ForkWorker(c.WorkerOptions{
		Name: "My HTTP Server",
		Action: func(_ context.Context) error {
			err := server.ListenAndServe()
			return err
		},
		RestartStrategy: c.Permanent,
	})

	// We need to keep track of the shutdown in another worker, given that
	// ListenAndServer will _always_ block
	_ = sup.ForkWorker(c.WorkerOptions{
		Name: "My HTTP Server Shutdown",
		Action: func(ctx context.Context) error {
			<-ctx.Done()
			if err := server.Shutdown(ctx); err != nil {
				return err
			} else {
				return ctx.Err()
			}
		},
		RestartStrategy: c.Transient,
	})

	// time.Sleep(1 * time.Second)
	// sup.CancelWorker(faulty)

	time.Sleep(3 * time.Second) // give leeway
	fmt.Println("Terminating this program")
	stopSup()
	fmt.Println("End of program")

}
